#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>


struct pixel {
    uint8_t b, g, r;
};
struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image set_image(uint64_t width, uint64_t height);
struct pixel get_pixel(const struct image* image, uint64_t x, uint64_t y);
void set_pixel (struct image* image, struct pixel pixel, uint64_t x, uint64_t y);
void free_image(struct image *image);
#endif
