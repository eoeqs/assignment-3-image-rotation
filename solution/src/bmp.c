#include "bmp.h"
#include <stdlib.h>

#define BF_TYPE 0x4D42
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_XPELS_PER_METER 1
#define BI_YPELS_PER_METER 1
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0
#define HEADER_SIZE sizeof(struct bmp_header)
#define PIXEL_SIZE sizeof(struct pixel)

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static uint8_t padding(uint64_t width) {
    return 4 - width * PIXEL_SIZE % 4;
}


struct bmp_header create_header(struct image const *image) {
    return (struct bmp_header) {
            .bfType = BF_TYPE,
            .bfileSize = HEADER_SIZE +
                         PIXEL_SIZE * (image->width * padding(image->width)) * image->height,
            .bfReserved = BF_RESERVED,
            .bOffBits = HEADER_SIZE,
            .biSize = BI_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = BI_COMPRESSION,
            .biSizeImage = image->height * (image->width + padding(image->width)),
            .biXPelsPerMeter = BI_XPELS_PER_METER,
            .biYPelsPerMeter = BI_YPELS_PER_METER,
            .biClrUsed = BI_CLR_USED,
            .biClrImportant = BI_CLR_IMPORTANT
    };
}

static struct image construct_image(const uint64_t width, const uint64_t height) {
    struct pixel *pixel = malloc(width * height * PIXEL_SIZE);
    if (pixel == NULL) {
        fprintf(stderr, "Failed to allocate memory for pixels\n");
        exit(EXIT_FAILURE);
    }
    return (struct image) {
            .height = height,
            .width = width,
            .data = pixel};
}

enum write_status to_bmp(FILE *out, struct image const *image) {
    struct bmp_header bmpHeader = create_header(image);
    if (!fwrite(&bmpHeader, HEADER_SIZE, 1, out)) {
        return WRITE_ERROR;
    }

    for (uint64_t i = 0; i < image->height; i++) {
        if (fwrite(image->data + i * image->width, PIXEL_SIZE, image->width, out) != image->width) {
            return WRITE_ERROR;
        }
        if (fseek(out, padding(image->width), SEEK_CUR)) {
            return WRITE_ERROR;
        }

    }

    return WRITE_OK;
}


enum read_status from_bmp(FILE *in, struct image *image) {
    struct bmp_header bmpHeader = {0};
    size_t header_read = fread(&bmpHeader, HEADER_SIZE, 1, in);
    if (header_read != 1) {
        return READ_INVALID_HEADER;
    }

    *image = construct_image(bmpHeader.biWidth, bmpHeader.biHeight);

    for (uint64_t i = 0; i < image->height; i++) {
        size_t pixels_read = fread(image->data + i * image->width, PIXEL_SIZE, image->width, in);
        if (pixels_read != image->width) {
            fprintf(stderr, "Failed to read pixel data for row %llu\n", (unsigned long long)i);
            return READ_ERROR;

        }
        if(fseek(in, padding(bmpHeader.biWidth), SEEK_CUR) != 0){
            fprintf(stderr, "Failed to seek past padding for row %llu\n", (unsigned long long)i);
            return READ_ERROR;
        }
    }

    if (bmpHeader.bfType != BF_TYPE) {
        return READ_INVALID_SIGNATURE;
    }

    if (bmpHeader.biBitCount != BI_BIT_COUNT) {
        return READ_INVALID_BITS;
    }

    if (bmpHeader.biWidth <= 0 || bmpHeader.biHeight <= 0) {
        return READ_INVALID_HEADER;
    }


    return READ_OK;
}




