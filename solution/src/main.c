#include "file_manager.h"
#include "bmp.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

#define RQ_ARGS_COUNT 4
#define ANGLE_IND 3
#define OUTPUT_FILE_IND 2
#define INPUT_FILE_IND 1

int main(int argc, char **argv) {
    if (argc != RQ_ARGS_COUNT) {
        fprintf(stderr, "3 args expected");
        return EXIT_FAILURE;
    }

    (void) argc;
    (void) argv;
    char *endptr;
    long angle = strtol(argv[ANGLE_IND], &endptr, 10);
    FILE *input = NULL;
    FILE *output = NULL;

    struct image source;


    if (!(open_file(&input, argv[INPUT_FILE_IND], "rb"))) {
        fprintf(stderr, "couldn't read input file");
        return EXIT_FAILURE;
    }

    if (!(open_file(&output, argv[OUTPUT_FILE_IND], "wb"))) {
        fprintf(stderr, "couldn't open output file");
        return EXIT_FAILURE;
    }

    if (from_bmp(input, &source) != READ_OK) {
        fprintf(stderr, "couldn't open bmp file");
        return EXIT_FAILURE;
    }

    struct image res = rotate_custom(source, angle);
    if (to_bmp(output, &res) != WRITE_OK) {
        fprintf(stderr, "couldn't save bmp file");
        return EXIT_FAILURE;
    }

    free_image(&source);
    free_image(&res);
    close_file(input);
    close_file(output);


    return 0;
}
