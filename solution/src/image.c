#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>


struct image set_image(const uint64_t width, const uint64_t height) {
    struct image image = {
            .data=malloc(width * height * sizeof(struct pixel)),
            .width = width,
            .height = height
    };
    if (image.data == NULL) {
        fprintf(stderr, "Failed to allocate memory for image data\n");
        exit(EXIT_FAILURE);
    }
    return image;
}

struct pixel get_pixel(const struct image *image, uint64_t x, uint64_t y) {
    return image->data[x + (y * image->width)];
}

void set_pixel(struct image *image, struct pixel pixel, uint64_t x, uint64_t y) {
    if (x < image->width && y < image->height) {
        image->data[x + (y * image->width)] = pixel;
    } else {
        fprintf(stderr, "Invalid coordinates: x=%llu, y=%llu\n", (unsigned long long)x, (unsigned long long)y);
    }

}

void free_image(struct image *image) {
    free(image->data);
}
