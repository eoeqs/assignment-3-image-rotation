#include "../include/rotate.h"
#include "image.h"
#include <stdio.h>



struct image rotate_custom(struct image source, long angle) {
    if (angle % 90 != 0 || angle < -270 || angle > 270) {
        fprintf(stderr, "Invalid angle: %ld\n", angle);
        return source;
    }

    long rotations = angle / 90;

    rotations = (rotations % 4 + 4) % 4;

    uint64_t width = source.width;
    uint64_t height = source.height;
    struct image res;

    if (rotations % 2 == 0) {
        res = set_image(width, height);
    } else {
        res = set_image(height, width);
    }

    for (uint64_t i = 0; i < height; i++) {
        for (uint64_t j = 0; j < width; j++) {
            struct pixel pixel = get_pixel(&source, j, i);
            switch (rotations) {
                case 0:
                    set_pixel(&res, pixel, j, i);
                    break;
                case 1:
                    set_pixel(&res, pixel, i, width - j - 1);
                    break;
                case 2:
                    set_pixel(&res, pixel, width - j - 1, height - i - 1);
                    break;
                case 3:
                    set_pixel(&res, pixel, height - i - 1, j);
                    break;
                default:
                    break;
            }
        }
    }
    return res;
}

